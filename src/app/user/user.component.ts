import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {User} from './user';


@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs: ['user']
})
export class UserComponent implements OnInit {
  user:User;

@Output()deleteEvant = new EventEmitter<User>();
@Output()editEvant = new EventEmitter<User>();

 
isEdit:Boolean = false;
editButtonText = 'Edit';
  
constructor() { }
sendDelete(){
    this.deleteEvant.emit(this.user);
}
  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
    if(!this.isEdit){
      this.editEvant.emit(this.user);
    }
  }
 
   ngOnInit() {   }
}