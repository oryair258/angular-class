import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()

export class UsersService {
  //private _url='http://jsonplaceholder.typicode.com/users';
  private _url='http://orya.myweb.jce.ac.il/api/users';
   
  usersObservable;

    getUsers(){
      this.usersObservable =this.af.database.list('/users');
      return this.usersObservable;
    } 

    getUsersFromApi(){
       return this._http.get(this._url).map(res =>res.json())
    }


    addUser(user){
    this.usersObservable.push(user);
    }

    updateUser(user){
      let userKey = user.$key;
      let userdate = {name: user.name, email:user.email}
      this.af.database.object('/users/'+ userKey).update(userdate);
    }

    deleteUser(user){
      let userKey = user.$key;
      this.af.database.object('/users/'+ userKey).remove();
    }
    
  constructor(private af:AngularFire, private _http:Http) { }

}
